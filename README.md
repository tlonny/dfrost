# `dfrost`

Frost is a CLI that allows you to easily backup and sync your media using AWS S3 Deep Archive. The benefits of Deep Archive are that of incredibly low costs (~$1 per TB), however - media only becomes available ~48 hours after initially requested. Thus, this solution isn't suitable for rapid syncing across multiple devices, but provides a secure + long term solution that _eventually_ becomes consistent.

## Setup

Installation is simple:

```
pip install dfrost
```

First, we must create a `.dfrost.json` configuration file inside our home directory. It should look like this:

```json
{
    "storage_bucket": "<name of s3 bucket used>",
    "sync_dir": "<path of local directory we wish to sync>",
    "publish_list": ["<gitignore style pathspecs of files we want to push to S3>"],
    "request_list": ["<gitignore style pathspecs of files we want to pull from S3>"]
}
```

## Usage

Simply run `dfrost push` to start pushing all publishable media to S3. We use the last modified time (vs. a checksum) to prevent redundant uploads.

To pull media from S3, we similarly just run `dfrost pull`. Be advised that if this is our first invocation of `dfrost pull`, no media is likely to be downloaded, as we will need to wait ~48 hours for the it to become available. By re-running `dfrost pull`, we pull any media that _has_ become available. 

It is suggested that `dfrost push` and `dfrost pull` are run on a cron schedule of once a day.